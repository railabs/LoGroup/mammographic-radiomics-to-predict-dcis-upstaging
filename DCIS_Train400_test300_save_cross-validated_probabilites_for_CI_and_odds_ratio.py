## Created Date: May 16, 2021
## Name: Rui Hou
## Study: DCIS Upstaging with Radiomics
## Requirement: Python 2.7

import sys,os,random,math,time,glob,csv,scipy.io,copy,xlrd,datetime,xlwt
import numpy as np
from scipy import interp
# import matplotlib.pyplot as plt
from itertools import cycle
from sklearn import svm, metrics, linear_model,preprocessing
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import RandomizedLogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from functools import reduce
from sklearn.feature_selection import RFE
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
warnings.filterwarnings("ignore", category=RuntimeWarning)
warnings.filterwarnings("ignore", category=FutureWarning)

def mean_std_normalize(X_new_tr,X_new_val):
	mean_std_scaler0 = preprocessing.StandardScaler(copy=True, with_mean=True, with_std=True)
	X_tr = mean_std_scaler0.fit_transform(X_new_tr)
	if len(X_new_val)>0:
		X_val = mean_std_scaler0.transform(X_new_val)
	else:
		X_val = []
	return X_tr,X_val

iterTimes = int(sys.argv[1]) if len(sys.argv==2) else 200 #Cross-validation repeat times. Default: 200
########### Load Features From CSV ###########
r_csv_path = 'normalized_features_csv/'
num_of_features_cv = 109
f_train_dcis400_cv_and_cli = open(r_csv_path + 'Train_400_features_CV_and_CLINICAL.csv','r')
f_test_dcis300_cv_and_cli = open(r_csv_path + 'Test_300_features_CV_and_CLINICAL.csv','r')

rea_dcis400_cv_and_cli = csv.reader(f_train_dcis400_cv_and_cli)
header_dcis400_cv_and_cli = next(rea_dcis400_cv_and_cli)
feat_only_all_dcis400_cv_and_cli = []
for one_row in rea_dcis400_cv_and_cli:
	feat_only_all_dcis400_cv_and_cli.append(one_row)

f_train_dcis400_cv_and_cli.close()
###
rea_dcis300_cv_and_cli = csv.reader(f_test_dcis300_cv_and_cli)
header_dcis300_cv_and_cli = next(rea_dcis300_cv_and_cli)
feat_only_all_dcis300_cv_and_cli = []
for one_row in rea_dcis300_cv_and_cli:
	feat_only_all_dcis300_cv_and_cli.append(one_row)

f_test_dcis300_cv_and_cli.close()

num_of_features_cv_and_cli = len(header_dcis400_cv_and_cli)-2
num_of_features_cli = num_of_features_cv_and_cli - num_of_features_cv
##
new_feat_list_cv_and_cli = [ii.replace('.',' ') for ii in header_dcis400_cv_and_cli[2:2+num_of_features_cv_and_cli]]
new_feat_list_cv = [ii.replace('.',' ') for ii in header_dcis400_cv_and_cli[2:2+num_of_features_cv]]
new_feat_list_cli = [ii.replace('.',' ') for ii in header_dcis400_cv_and_cli[2+num_of_features_cv:]]
########################################################################################################################################
idx_dx = [idss for idss,he in enumerate(header_dcis400_cv_and_cli) if he=='Label'][0]
X_training_400_cv_and_cli = np.array([]).reshape(0,num_of_features_cv_and_cli)
Y_training_400 = np.array([]).astype(np.uint8)
for idss,one_row in enumerate(feat_only_all_dcis400_cv_and_cli):
	one_feat_now = np.array([float(ii) for ii in one_row[2:num_of_features_cv_and_cli+2]]).reshape((1,num_of_features_cv_and_cli))
	one_dx_now = np.array([int(one_row[idx_dx])])
	X_training_400_cv_and_cli = np.concatenate((X_training_400_cv_and_cli,one_feat_now),axis = 0)
	Y_training_400 = np.concatenate((Y_training_400,one_dx_now),axis = 0)

########################################################################################################################################
idx_dx = [idss for idss,he in enumerate(header_dcis300_cv_and_cli) if he=='Label'][0]
X_testing_300_cv_and_cli = np.array([]).reshape(0,num_of_features_cv_and_cli)
Y_testing_300 = np.array([]).astype(np.uint8)
for idss,one_row in enumerate(feat_only_all_dcis300_cv_and_cli):
	one_feat_now = np.array([float(ii) for ii in one_row[2:num_of_features_cv_and_cli+2]]).reshape((1,num_of_features_cv_and_cli))
	one_dx_now = np.array([int(one_row[idx_dx])])
	X_testing_300_cv_and_cli = np.concatenate((X_testing_300_cv_and_cli,one_feat_now),axis = 0)
	Y_testing_300 = np.concatenate((Y_testing_300,one_dx_now),axis = 0)

X_training_400_cv_and_cli = X_training_400_cv_and_cli.astype(np.float32)
X_testing_300_cv_and_cli = X_testing_300_cv_and_cli.astype(np.float32)
###########################
nfold = 5
embedded_cv = 4
pow_base_c = 10
c_min = -10
c_max = 10
Cs = [pow(pow_base_c,i) for i in range(c_min,c_max)]
param_grid = {'C': Cs}
#####
####################################################With FOUR Clinical Features Only###########################################################
X_training_400_cli = np.copy(X_training_400_cv_and_cli[:,num_of_features_cv:])

saved_log_path  = 'saved_logs_predictions_and_labels/'
f_log = open(saved_log_path + 'logs_FOUR_CLINICAL_Features_Only_save_predictions_LogisticRegression.log','a')
best_c_training_400_cli = []
auc_training_400_cli_nofs = np.zeros([iterTimes,nfold])
##
num_training_pure = np.where(Y_training_400==0)[0].shape[0]
num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
num_holdout_pure = int(round(num_training_pure/float(nfold)))
num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
##
f_csv_cli_only_labels = open(saved_log_path + 'CLINICAL_FOUR_only_Labels_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
wri_cli_only_labels = csv.writer(f_csv_cli_only_labels)
f_csv_cli_only_preds = open(saved_log_path + 'CLINICAL_FOUR_only_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
wri_cli_only_preds = csv.writer(f_csv_cli_only_preds)
##
ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
	random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
	random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
	for nf in range(nfold):
		ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
		ind_tr = list(set(ind0+ind1)-set(ind_val))
		X_new_tr = np.copy(X_training_400_cli[np.array(ind_tr),:])
		Y_tr = np.array(Y_training_400[np.array(ind_tr)])
		X_new_val = np.copy(X_training_400_cli[np.array(ind_val),:])
		Y_val = np.array(Y_training_400[np.array(ind_val)])
		wri_cli_only_labels.writerow(Y_val.tolist())
		X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
		lr_baseline_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = -1)
		_=lr_baseline_infold.fit(X_tr,Y_tr)
		best_c_now = lr_baseline_infold.best_params_['C']
		best_c_training_400_cli.extend([best_c_now])
		val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
		wri_cli_only_preds.writerow(val_proba_now.tolist())
		auc_training_400_cli_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
	print('------Run %d With Four Clinical Features AUC: %0.3f +- %0.3f' %(iter2+1, round(auc_training_400_cli_nofs[:iter2+1].mean(),3),round(auc_training_400_cli_nofs[:iter2+1].mean(1).std(),3)))
	f_log.write('------Run %d With Four Clinical Features AUC: %0.3f +- %0.3f\n' %(iter2+1, round(auc_training_400_cli_nofs[:iter2+1].mean(),3),round(auc_training_400_cli_nofs[:iter2+1].mean(1).std(),3)))

print('With Four Clinical Features AUC: %0.3f +- %0.3f' %(round(auc_training_400_cli_nofs[:iter2+1].mean(),3),round(auc_training_400_cli_nofs[:iter2+1].mean(1).std(),3)))
f_log.write('With Four Clinical Features AUC: %0.3f +- %0.3f\n' %(round(auc_training_400_cli_nofs[:iter2+1].mean(),3),round(auc_training_400_cli_nofs[:iter2+1].mean(1).std(),3)))

np.save(saved_log_path + 'CLINICAL_FOUR_only_Training_NO-FS_best_parameters_C.npy',best_c_training_400_cli)
f_log.close()

####################################################With Clinical Invidivual Clinical Features Only###########################################################
X_training_400_cli = np.copy(X_training_400_cv_and_cli[:,num_of_features_cv:])

num_training_pure = np.where(Y_training_400==0)[0].shape[0]
num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
num_holdout_pure = int(round(num_training_pure/float(nfold)))
num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
for selected_cli_feat_idx, current_feat_picked in enumerate(new_feat_list_cli):
	X_training_400_one_cli = np.copy(X_training_400_cli[:,selected_cli_feat_idx:(selected_cli_feat_idx+1)])
	f_csv_one_cli_only_labels = open(saved_log_path + '%s_only_Labels_with_%dfolds_%diterations.csv'%(current_feat_picked.replace(' ','_'), nfold,iterTimes),'w')
	wri_one_cli_only_labels = csv.writer(f_csv_one_cli_only_labels)
	f_csv_one_cli_only_preds = open(saved_log_path + '%s_only_Predictions_with_%dfolds_%diterations.csv'%(current_feat_picked.replace(' ','_'), nfold,iterTimes),'w')
	wri_one_cli_only_preds = csv.writer(f_csv_one_cli_only_preds)
	##
	best_c_training_400_one_cli = []
	auc_training_400_one_cli = np.zeros([iterTimes,nfold])
	##
	ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
	ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
	for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
		random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
		random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
		for nf in range(nfold):
			ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
			ind_tr = list(set(ind0+ind1)-set(ind_val))
			X_new_tr = np.copy(X_training_400_one_cli[np.array(ind_tr),:])
			Y_tr = np.array(Y_training_400[np.array(ind_tr)])
			X_new_val = np.copy(X_training_400_one_cli[np.array(ind_val),:])
			Y_val = np.array(Y_training_400[np.array(ind_val)])
			wri_one_cli_only_labels.writerow(Y_val.tolist())
			X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
			lr_baseline_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = -1)
			_=lr_baseline_infold.fit(X_tr,Y_tr)
			best_c_now = lr_baseline_infold.best_params_['C']
			best_c_training_400_one_cli.extend([best_c_now])
			val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
			wri_one_cli_only_preds.writerow(val_proba_now.tolist())
			auc_training_400_one_cli[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
		print('------With One %s Clinical Feat, Run %d AUC: %0.3f +- %0.3f' %(current_feat_picked, iter2+1, round(auc_training_400_one_cli[:iter2+1].mean(),3),round(auc_training_400_one_cli[:iter2+1].mean(1).std(),3)))
		f_log.write('------With One %s Clinical Feat, Run %d AUC: %0.3f +- %0.3f\n' %(current_feat_picked, iter2+1, round(auc_training_400_one_cli[:iter2+1].mean(),3),round(auc_training_400_one_cli[:iter2+1].mean(1).std(),3)))
	print('With One %s Clinical Feat,AUC: %0.3f +- %0.3f' %(current_feat_picked, round(auc_training_400_one_cli.mean(),3),round(auc_training_400_one_cli.mean(1).std(),3)))
	f_log.write('With One %s Clinical Feat, AUC: %0.3f +- %0.3f\n' %(current_feat_picked, round(auc_training_400_one_cli.mean(),3),round(auc_training_400_one_cli.mean(1).std(),3)))
	f_csv_one_cli_only_labels.close()
	f_csv_one_cli_only_preds.close()

################################CV ONLY Logistic Regression with Feature Selection and Top Features Performance######################
X_training_400_cv = np.copy(X_training_400_cv_and_cli[:,:num_of_features_cv])

saved_log_path  = 'saved_logs_predictions_and_labels/'
f_log = open(saved_log_path + 'logs_CV_ONLY_LogisticRegression_WITH_and_WITHOUT_feature_selection_with_saving_predictions.log','a')
## Radiomics Only With Repeated Cross-Validation with Logistic Regression
num_training_pure = np.where(Y_training_400==0)[0].shape[0]
num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
num_holdout_pure = int(round(num_training_pure/float(nfold)))
num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
##
f_csv_cv_labels = open(saved_log_path + 'CV_ONLY_Labels_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
wri_cv_labels = csv.writer(f_csv_cv_labels)
##
best_c_training_400_cv = []
auc_training_400_cv_nofs = np.zeros([iterTimes,nfold])
f_csv_cv_nofs = open(saved_log_path + 'CV_ONLY_NO-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
wri_cv_nofs = csv.writer(f_csv_cv_nofs)
##
best_c_training_400_cv_with_fs = []
auc_training_400_cv_with_fs = np.zeros([iterTimes,nfold])
f_csv_cv_with_fs = open(saved_log_path + 'CV_ONLY_WITH-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
wri_cv_with_fs = csv.writer(f_csv_cv_with_fs)
##
idx_feat_picked_training_400 = []
feat_picked_training_400 = []
ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
	random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
	random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
	for nf in range(nfold):
		ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
		ind_tr = list(set(ind0+ind1)-set(ind_val))
		X_new_tr = np.copy(X_training_400_cv[np.array(ind_tr),:])
		Y_tr = np.array(Y_training_400[np.array(ind_tr)])
		X_new_val = np.copy(X_training_400_cv[np.array(ind_val),:])
		Y_val = np.array(Y_training_400[np.array(ind_val)])
		wri_cv_labels.writerow(Y_val.tolist())
		X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
		lr_baseline_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = -1)
		_=lr_baseline_infold.fit(X_tr,Y_tr)
		best_c_now = lr_baseline_infold.best_params_['C']
		best_c_training_400_cv.extend([best_c_now])
		val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
		wri_cv_nofs.writerow(val_proba_now.tolist())
		auc_training_400_cv_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
		##
		randomized_logistic = RandomizedLogisticRegression(C=1,random_state=nfold)
		randomized_logistic.fit(X_tr,Y_tr)
		ind_fea = np.array([i[0] for i in randomized_logistic.all_scores_])
		rank_one_ind_fea = np.flipud(np.argsort(ind_fea))
		current_idx_feat_picked = [i for i in rank_one_ind_fea if ind_fea[i]>0]
		current_feat_picked = np.array(new_feat_list_cv)[np.array(current_idx_feat_picked)]
		idx_feat_picked_training_400.append(current_idx_feat_picked)
		feat_picked_training_400.append(current_feat_picked)
		X_tr = np.copy(X_new_tr[:,np.array(current_idx_feat_picked)])
		X_val = np.copy(X_new_val[:,np.array(current_idx_feat_picked)])
		lr_training_400_infold_with_fs = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = -1)
		_=lr_training_400_infold_with_fs.fit(X_tr,Y_tr)
		best_c_now = lr_training_400_infold_with_fs.best_params_['C']
		best_c_training_400_cv_with_fs.extend([best_c_now])
		val_proba_now_with_fs = lr_training_400_infold_with_fs.predict_proba(X_val)[:,1]
		wri_cv_with_fs.writerow(val_proba_now_with_fs.tolist())
		auc_training_400_cv_with_fs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now_with_fs)
	print('----CV Only Logistic Regression Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f' %(iter2+1, round(auc_training_400_cv_nofs[:iter2+1].mean(),3),round(auc_training_400_cv_nofs[:iter2+1].mean(1).std(),3),round(auc_training_400_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cv_with_fs[:iter2+1].mean(1).std(),3)))
	f_log.write('----CV Only Logistic Regression Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f\n' %(iter2+1, round(auc_training_400_cv_nofs[:iter2+1].mean(),3),round(auc_training_400_cv_nofs[:iter2+1].mean(1).std(),3),round(auc_training_400_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cv_with_fs[:iter2+1].mean(1).std(),3)))

np.save(saved_log_path+'CV_ONLY_Training_NO_FS_400_LogisticRegression_c.npy',best_c_training_400_cv)
np.save(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_c.npy',best_c_training_400_cv_with_fs)
np.save(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy',idx_feat_picked_training_400)

f_feat_picked = open(saved_log_path + 'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_names.txt','w')
for one_select in feat_picked_training_400:
	f_feat_picked.write(' + '.join(one_select)+'\n')
f_feat_picked.close()

f_log.close()
f_csv_cv_labels.close()
f_csv_cv_nofs.close()
f_csv_cv_with_fs.close()

######################################

restarted_num_feat = 1
new_feat_list_cv = header_dcis400_cv_and_cli[2:2+num_of_features_cv]
X_training_400_cv = np.copy(X_training_400_cv_and_cli[:,:num_of_features_cv])
saved_log_path  = 'saved_logs_predictions_and_labels/'
f_log = open(saved_log_path + 'logs_CV_ONLY_Features_LogisticRegression_TOP_features.log','a')

idx_feat_picked_modelA_400 = np.load(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy')
flatten_idx_feat_picked_modelA_400 = []
for one_select in idx_feat_picked_modelA_400:
	flatten_idx_feat_picked_modelA_400.extend(one_select)

uni_feat,counts_feat = np.unique(flatten_idx_feat_picked_modelA_400,return_counts = True)
sorted_feat = uni_feat[np.flipud(np.argsort(counts_feat))]
sorted_counts = np.flipud(np.sort(counts_feat))
##

num_training_pure = np.where(Y_training_400==0)[0].shape[0]
num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
num_holdout_pure = int(round(num_training_pure/float(nfold)))
num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
for selected_features_num in range(restarted_num_feat,sorted_feat.shape[0]+1):
	X_training_400_subset = np.copy(X_training_400_cv[:,sorted_feat[:selected_features_num]])
	current_feat_picked = np.array(new_feat_list_cv)[sorted_feat[:selected_features_num]].tolist()
	best_c_training_400_cv = []
	auc_training_400_cv_nofs = np.zeros([iterTimes,nfold])
	##
	ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
	ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
	for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
		random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
		random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
		for nf in range(nfold):
			ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
			ind_tr = list(set(ind0+ind1)-set(ind_val))
			X_new_tr = np.copy(X_training_400_subset[np.array(ind_tr),:])
			Y_tr = np.array(Y_training_400[np.array(ind_tr)])
			X_new_val = np.copy(X_training_400_subset[np.array(ind_val),:])
			Y_val = np.array(Y_training_400[np.array(ind_val)])
			X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
			lr_baseline_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = 20)
			_=lr_baseline_infold.fit(X_tr,Y_tr)
			best_c_now = lr_baseline_infold.best_params_['C']
			best_c_training_400_cv.extend([best_c_now])
			val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
			auc_training_400_cv_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
		print('-------With Top %d features, Run %d AUC: %0.3f +- %0.3f' %(selected_features_num, iter2+1, round(auc_training_400_cv_nofs[:iter2+1].mean(),3),round(auc_training_400_cv_nofs[:iter2+1].mean(1).std(),3)))
		f_log.write('-------With Top %d features, Run %d AUC: %0.3f +- %0.3f\n' %(selected_features_num, iter2+1, round(auc_training_400_cv_nofs[:iter2+1].mean(),3),round(auc_training_400_cv_nofs[:iter2+1].mean(1).std(),3)))
	print('With Top %d features AUC: %0.3f +- %0.3f,features: %s' %(selected_features_num, round(auc_training_400_cv_nofs.mean(),3),round(auc_training_400_cv_nofs.mean(1).std(),3),' + '.join(current_feat_picked)))
	f_log.write('With Top %d features AUC: %0.3f +- %0.3f,features: %s\n' %(selected_features_num, round(auc_training_400_cv_nofs.mean(),3),round(auc_training_400_cv_nofs.mean(1).std(),3),' + '.join(current_feat_picked)))

f_log.close()

#################################################################################################################################################################
selected_features_num = 11
	X_training_400_cv = np.copy(X_training_400_cv_and_cli[:,:num_of_features_cv])

saved_log_path  = 'saved_logs_predictions_and_labels/'
f_log = open(saved_log_path + 'logs_CV_ONLY_selected_TOP_%dFeatures_saving_predictions.log'%(selected_features_num),'a')
##
f_csv_cv_labels = open(saved_log_path + 'With_%dTOP_features_from_CV_feature_selection_labels.csv'%(selected_features_num),'w')
wri_cv_labels = csv.writer(f_csv_cv_labels)
best_c_training_400_cv_with_fs = []
auc_training_400_cv_with_fs = np.zeros([iterTimes,nfold])
f_csv_cv_with_fs = open(saved_log_path + 'With_%dTOP_features_from_CV_feature_selection_predictions.csv'%(selected_features_num),'w')
wri_cv_with_fs = csv.writer(f_csv_cv_with_fs)
##
idx_feat_picked_modelA_400 = np.load(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy')
flatten_idx_feat_picked_modelA_400 = []
for one_select in idx_feat_picked_modelA_400:
	flatten_idx_feat_picked_modelA_400.extend(one_select)

uni_feat,counts_feat = np.unique(flatten_idx_feat_picked_modelA_400,return_counts = True)
sorted_feat = uni_feat[np.flipud(np.argsort(counts_feat))]
sorted_counts = np.flipud(np.sort(counts_feat))

X_training_400_subset = np.copy(X_training_400_cv[:,sorted_feat[:selected_features_num]])
current_feat_picked = np.array(new_feat_list_cv)[sorted_feat[:selected_features_num]].tolist()
print('Features:',current_feat_picked)
##
num_training_pure = np.where(Y_training_400==0)[0].shape[0]
num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
num_holdout_pure = int(round(num_training_pure/float(nfold)))
num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]

for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
	random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
	random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
	for nf in range(nfold):
		ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
		ind_tr = list(set(ind0+ind1)-set(ind_val))
		X_new_tr = np.copy(X_training_400_subset[np.array(ind_tr),:])
		Y_tr = np.array(Y_training_400[np.array(ind_tr)])
		X_new_val = np.copy(X_training_400_subset[np.array(ind_val),:])
		Y_val = np.array(Y_training_400[np.array(ind_val)])
		wri_cv_labels.writerow(Y_val.tolist())
		X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
		lr_baseline_infold_with_fs = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = -1)
		_=lr_baseline_infold_with_fs.fit(X_tr,Y_tr)
		best_c_now = lr_baseline_infold_with_fs.best_params_['C']
		best_c_training_400_cv_with_fs.extend([best_c_now])
		val_proba_now_with_fs = lr_baseline_infold_with_fs.predict_proba(X_val)[:,1]
		wri_cv_with_fs.writerow(val_proba_now_with_fs.tolist())
		auc_training_400_cv_with_fs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now_with_fs)
			####
	print('------Feature Num: %d, Logistic Regression Run %d  auc: %0.3f +- %0.3f' %(selected_features_num,iter2+1, round(auc_training_400_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cv_with_fs[:iter2+1].mean(1).std(),3)))
	f_log.write('------Feature Num: %d, Logistic Regression Run %d auc: %0.3f +- %0.3f\n' %(selected_features_num, iter2+1, round(auc_training_400_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cv_with_fs[:iter2+1].mean(1).std(),3)))

print('%d Number of TOP Features, Logistic Regression auc: %0.3f +- %0.3f, with Features: %s' %(selected_features_num, round(auc_training_400_cv_with_fs.mean(),3),round(auc_training_400_cv_with_fs.mean(1).std(),3),current_feat_picked))
f_log.write('%d Number of TOP Features, Logistic Regression auc: %0.3f +- %0.3f, with Features: %s\n' %(selected_features_num, round(auc_training_400_cv_with_fs.mean(),3),round(auc_training_400_cv_with_fs.mean(1).std(),3),current_feat_picked))
##
np.save(saved_log_path + 'With_%dTOP_features_from_CV_feature_selection_best_parameters_C.npy'%(selected_features_num),best_c_training_400_cv_with_fs)
f_csv_cv_labels.close()
f_csv_cv_with_fs.close()

########################################################################################################################################

#########################CV AND CLINICAL FEATURES Logistic Regression with Feature Selection and Top Features Performance###############

saved_log_path  = 'saved_logs_predictions_and_labels/'
f_log = open(saved_log_path + 'logs_CLINICAL_plus_CV_LR_WITH_and_WITHOUT_feature_selection.log','a')

## Model A on 400 Training Data with Logistic Regression
num_training_pure = np.where(Y_training_400==0)[0].shape[0]
num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
num_holdout_pure = int(round(num_training_pure/float(nfold)))
num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
##
f_csv_cv_and_cli_labels = open(saved_log_path + 'CV_AND_Clinical_Labels_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
wri_cv_and_cli_labels = csv.writer(f_csv_cv_and_cli_labels)
##
best_c_training_400_cli_and_cv = []
auc_training_400_cli_and_cv_nofs = np.zeros([iterTimes,nfold])
f_csv_cv_and_cli_nofs = open(saved_log_path + 'CV_AND_Clinical_NO-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
wri_cv_and_cli_nofs = csv.writer(f_csv_cv_and_cli_nofs)
##
best_c_training_400_cli_and_cv_with_fs = []
auc_training_400_cli_and_cv_with_fs = np.zeros([iterTimes,nfold])
f_csv_cv_and_cli_with_fs = open(saved_log_path + 'CV_AND_Clinical_WITH-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
wri_cv_and_cli_with_fs = csv.writer(f_csv_cv_and_cli_with_fs)
##
idx_feat_picked_training_400 = []
feat_picked_training_400 = []
ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
	random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
	random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
	for nf in range(nfold):
		ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
		ind_tr = list(set(ind0+ind1)-set(ind_val))
		X_new_tr = np.copy(X_training_400_cv_and_cli[np.array(ind_tr),:])
		Y_tr = np.array(Y_training_400[np.array(ind_tr)])
		X_new_val = np.copy(X_training_400_cv_and_cli[np.array(ind_val),:])
		Y_val = np.array(Y_training_400[np.array(ind_val)])
		wri_cv_and_cli_labels.writerow(Y_val.tolist())
		X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
		lr_baseline_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = -1)
		_=lr_baseline_infold.fit(X_tr,Y_tr)
		best_c_now = lr_baseline_infold.best_params_['C']
		best_c_training_400_cli_and_cv.extend([best_c_now])
		val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
		wri_cv_and_cli_nofs.writerow(val_proba_now.tolist())
		auc_training_400_cli_and_cv_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
		##
		randomized_logistic = RandomizedLogisticRegression(C=1,random_state=nfold)
		randomized_logistic.fit(X_tr,Y_tr)
		ind_fea = np.array([i[0] for i in randomized_logistic.all_scores_])
		rank_one_ind_fea = np.flipud(np.argsort(ind_fea))
		current_idx_feat_picked = [i for i in rank_one_ind_fea if ind_fea[i]>0]
		current_feat_picked = np.array(new_feat_list_cv_and_cli)[np.array(current_idx_feat_picked)]
		idx_feat_picked_training_400.append(current_idx_feat_picked)
		feat_picked_training_400.append(current_feat_picked)
		X_tr = np.copy(X_new_tr[:,np.array(current_idx_feat_picked)])
		X_val = np.copy(X_new_val[:,np.array(current_idx_feat_picked)])
		lr_training_400_infold_with_fs = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = -1)
		_=lr_training_400_infold_with_fs.fit(X_tr,Y_tr)
		best_c_now = lr_training_400_infold_with_fs.best_params_['C']
		best_c_training_400_cli_and_cv_with_fs.extend([best_c_now])
		val_proba_now_with_fs = lr_training_400_infold_with_fs.predict_proba(X_val)[:,1]
		wri_cv_and_cli_with_fs.writerow(val_proba_now_with_fs.tolist())
		auc_training_400_cli_and_cv_with_fs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now_with_fs)
	print('----Clinical And CV Logistic Regression Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f' %(iter2+1, round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(),3),round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(1).std(),3),round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(1).std(),3)))
	f_log.write('----Clinical And CV Logistic Regression Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f\n' %(iter2+1, round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(),3),round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(1).std(),3),round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(1).std(),3)))

np.save(saved_log_path+'CV_AND_Clinical_Training_NO_FS_400_LogisticRegression_c.npy',best_c_training_400_cli_and_cv)
np.save(saved_log_path+'CV_AND_Clinical_Training_WITH_FS_400_LogisticRegression_c.npy',best_c_training_400_cli_and_cv_with_fs)
np.save(saved_log_path+'CV_AND_Clinical_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy',idx_feat_picked_training_400)

f_feat_picked = open(saved_log_path + 'Clinical_not_categorical_and_CV_Training_WITH_FS_400_LogisticRegression_picked_features_names.txt','w')
for one_select in feat_picked_training_400:
	f_feat_picked.write(' + '.join(one_select)+'\n')
f_feat_picked.close()

f_log.close()
f_csv_cv_and_cli_labels.close()
f_csv_cv_and_cli_nofs.close()
f_csv_cv_and_cli_with_fs.close()

###############################################################################################################################

restarted_num_feat = 1
num_of_features_cv_and_cli = len(header_dcis400_cv_and_cli)-2
new_feat_list_cv_and_cli = header_dcis400_cv_and_cli[2:2+num_of_features_cv_and_cli]
saved_log_path  = 'saved_logs_predictions_and_labels/'
f_log = open(saved_log_path + 'logs_CV_AND_Clinical_Features_LogisticRegression_TOP_features.log','a')


idx_feat_picked_modelA_400_cv_and_cli = np.load(saved_log_path+'CV_AND_Clinical_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy')
flatten_idx_feat_picked_modelA_400_cv_and_cli = []
for one_select in idx_feat_picked_modelA_400_cv_and_cli:
	flatten_idx_feat_picked_modelA_400_cv_and_cli.extend(one_select)

uni_feat_cv_and_cli,counts_feat_cv_and_cli = np.unique(flatten_idx_feat_picked_modelA_400_cv_and_cli,return_counts = True)
sorted_feat_cv_and_cli = uni_feat_cv_and_cli[np.flipud(np.argsort(counts_feat_cv_and_cli))]
sorted_counts_cv_and_cli = np.flipud(np.sort(counts_feat_cv_and_cli))

num_training_pure = np.where(Y_training_400==0)[0].shape[0]
num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
num_holdout_pure = int(round(num_training_pure/float(nfold)))
num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))

for selected_features_num in range(restarted_num_feat,sorted_feat_cv_and_cli.shape[0]+1):
	X_training_400_subset = np.copy(X_training_400_cv_and_cli[:,sorted_feat_cv_and_cli[:selected_features_num]])
	current_feat_picked = np.array(new_feat_list_cv_and_cli)[sorted_feat_cv_and_cli[:selected_features_num]].tolist()
	best_c_training_400_cv = []
	auc_training_400_cv_and_cli_nofs = np.zeros([iterTimes,nfold])
	##
	ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
	ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
	for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
		random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
		random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
		for nf in range(nfold):
			ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
			ind_tr = list(set(ind0+ind1)-set(ind_val))
			X_new_tr = np.copy(X_training_400_subset[np.array(ind_tr),:])
			Y_tr = np.array(Y_training_400[np.array(ind_tr)])
			X_new_val = np.copy(X_training_400_subset[np.array(ind_val),:])
			Y_val = np.array(Y_training_400[np.array(ind_val)])
			X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
			lr_baseline_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = 20)
			_=lr_baseline_infold.fit(X_tr,Y_tr)
			best_c_now = lr_baseline_infold.best_params_['C']
			best_c_training_400_cv.extend([best_c_now])
			val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
			auc_training_400_cv_and_cli_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
		print('-------With Top %d features, Run %d AUC: %0.3f +- %0.3f' %(selected_features_num, iter2+1, round(auc_training_400_cv_and_cli_nofs[:iter2+1].mean(),3),round(auc_training_400_cv_and_cli_nofs[:iter2+1].mean(1).std(),3)))
		f_log.write('-------With Top %d features, Run %d AUC: %0.3f +- %0.3f\n' %(selected_features_num, iter2+1, round(auc_training_400_cv_and_cli_nofs[:iter2+1].mean(),3),round(auc_training_400_cv_and_cli_nofs[:iter2+1].mean(1).std(),3)))
	print('With Top %d features AUC: %0.3f +- %0.3f,features: %s' %(selected_features_num, round(auc_training_400_cv_and_cli_nofs.mean(),3),round(auc_training_400_cv_and_cli_nofs.mean(1).std(),3),' + '.join(current_feat_picked)))
	f_log.write('With Top %d features AUC: %0.3f +- %0.3f,features: %s\n' %(selected_features_num, round(auc_training_400_cv_and_cli_nofs.mean(),3),round(auc_training_400_cv_and_cli_nofs.mean(1).std(),3),' + '.join(current_feat_picked)))

f_log.close()


#################################################################################################################################################################

####################### FINAL Testing, On Training Set#######################

###################### Extract Parameters AND Train on ALL TRAINING DATASET AGAIN ######################
selected_features_cv_only_num = 11
##
saved_log_path  = 'saved_logs_predictions_and_labels/'
new_feat_list_cv = [ii.replace('.',' ') for ii in header_dcis400_cv_and_cli[2:2+num_of_features_cv]]
X_training_400_cv = np.copy(X_training_400_cv_and_cli[:,:num_of_features_cv])
X_training_400_cli = np.copy(X_training_400_cv_and_cli[:,num_of_features_cv:])
case_names_train400 = [ii[0] for ii in feat_only_all_dcis400_cv_and_cli]
## DCIS-C
best_c_cli_only = np.load(saved_log_path + 'CLINICAL_FOUR_only_Training_NO-FS_best_parameters_C.npy')
uni_c_cli_only,counts_c_cli_only = np.unique(best_c_cli_only,return_counts = True)
most_selected_c_cli = uni_c_cli_only[np.flipud(np.argsort(counts_c_cli_only))][0]
lr_baseline_cli_only = linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000,C = most_selected_c_cli)
_=lr_baseline_cli_only.fit(X_training_400_cli,Y_training_400)
## DCIS-R
best_c_modela_400_cv_only = np.load(saved_log_path + 'CV_ONLY_Training_NO_FS_400_LogisticRegression_c.npy')
uni_c_cv_only,counts_c_cv_only = np.unique(best_c_modela_400_cv_only,return_counts = True)
most_selected_c_cv_only = uni_c_cv_only[np.flipud(np.argsort(counts_c_cv_only))][0]
lr_modela_cv_only_nofs = linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000,C = most_selected_c_cv_only)
_=lr_modela_cv_only_nofs.fit(X_training_400_cv,Y_training_400)
## DCIS-RC
best_c_modela_400_cli_and_cv = np.load(saved_log_path + 'CV_AND_Clinical_Training_NO_FS_400_LogisticRegression_c.npy')
uni_c_cli_and_c,counts_c_cli_and_c = np.unique(best_c_modela_400_cli_and_cv,return_counts = True)
most_selected_c_cv_and_cli = uni_c_cli_and_c[np.flipud(np.argsort(counts_c_cli_and_c))][0]
lr_modela_cv_and_cli_nofs = linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000,C = most_selected_c_cv_and_cli)
_=lr_modela_cv_and_cli_nofs.fit(X_training_400_cv_and_cli,Y_training_400)
## DCIS-Rs
best_c_training_400_cv_top_features = np.load(saved_log_path + 'With_%dTOP_features_from_CV_feature_selection_best_parameters_C.npy'%(selected_features_cv_only_num))
idx_feat_picked_modelA_400 = np.load(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy')
flatten_idx_feat_picked_modelA_400 = []
for one_select in idx_feat_picked_modelA_400:
  flatten_idx_feat_picked_modelA_400.extend(one_select)

uni_feat_cv,counts_feat_cv = np.unique(flatten_idx_feat_picked_modelA_400,return_counts = True)
sorted_feat_cv = uni_feat_cv[np.flipud(np.argsort(counts_feat_cv))]
sorted_counts_cv = np.flipud(np.sort(counts_feat_cv))
##
X_training_400_cv_subset = np.copy(X_training_400_cv[:,sorted_feat_cv[:selected_features_cv_only_num]])
uni_c_cv_only_topfeats,counts_c_cv_only_topfeats = np.unique(best_c_training_400_cv_top_features,return_counts = True)
most_selected_c_cv_only_top_feats = uni_c_cv_only_topfeats[np.flipud(np.argsort(counts_c_cv_only_topfeats))][0]
lr_modela_cv_only_topfeats = linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000,C = most_selected_c_cv_only_top_feats)
_=lr_modela_cv_only_topfeats.fit(X_training_400_cv_subset,Y_training_400)
## AUC Performance On TEST ############################
## AUC Performance On TEST ############################
saved_log_path  = 'saved_logs_predictions_and_labels/'
X_testing_300_cv = X_testing_300_cv_and_cli[:, :num_of_features_cv]
X_testing_300_cli = X_testing_300_cv_and_cli[:, num_of_features_cv:]
case_names_test300 = [ii[0] for ii in feat_only_all_dcis300_cv_and_cli]
X_testing_300_cv_subset = np.copy(X_testing_300_cv[:,sorted_feat_cv[:selected_features_cv_only_num]])
X_testing_300_cv_and_cli_subset = np.copy(X_testing_300_cv_and_cli[:,sorted_feat_cv_and_cli[:selected_features_cv_and_cli_num]])
## Model A + No FS on Clinical Features
proba_test_300_cli = lr_baseline_cli_only.predict_proba(X_testing_300_cli)[:,1]
auc_test_modela_cli_only = metrics.roc_auc_score(Y_testing_300,proba_test_300_cli)
## Model A + No FS on CV
proba_test_300_cv = lr_modela_cv_only_nofs.predict_proba(X_testing_300_cv)[:,1]
auc_test_modela_cv_nofs = metrics.roc_auc_score(Y_testing_300,proba_test_300_cv)
## Model A + No FS on CV + Clinical
proba_test_300_cv_and_cli = lr_modela_cv_and_cli_nofs.predict_proba(X_testing_300_cv_and_cli)[:,1]
auc_test_modela_cv_and_cli_nofs = metrics.roc_auc_score(Y_testing_300,proba_test_300_cv_and_cli) 
## Model A + Top Feats on CV 
# selected_features_cv_only_num = 11
proba_test_300_top_feats = lr_modela_cv_only_topfeats.predict_proba(X_testing_300_cv_subset)[:,1]
auc_test_modela_cv_only_top_feats = metrics.roc_auc_score(Y_testing_300,proba_test_300_top_feats) 
####
f_csv_final_test300 = open(saved_log_path + 'Final_Test300_from_train400_pred_and_labels_4models_cvtop%d_109CV_4Cli.csv'%(selected_features_cv_only_num),'w')
wri_csv_final_test300 = csv.writer(f_csv_final_test300)
wri_csv_final_test300.writerow(['ID','labels','pred.CV','pred.CV.plus.clinical','pred.top.feats','pred.cli.only'])

for idss in range(Y_testing_300.shape[0]):
	 wri_csv_final_test300.writerow([case_names_test300[idss],Y_testing_300[idss], proba_test_300_cv[idss], proba_test_300_cv_and_cli[idss], proba_test_300_top_feats[idss], proba_test_300_cli[idss]])
f_csv_final_test300.close()
