Code and models from the paper "_Prediction of upstaging in ductal carcinoma in situ based upon mammographic radiomic features_"

(More details will be added once manuscript get published)

**Platform**

For running machine learning models: **Python 2.7.12**

For statistical analysis: **R 4.0.2**

Steps:
1. Generate microcalcification masks from DICOM images.
   ```
    python generate_MCC_from_one_Unet.py one_dcm_full_path one_lesion_full_path one_breast_region_full_path 
    return binary_MC_mask

   ```

2. Extract radiomics from DICOM images.
    ```
    python radiomics_feature_extraction_direct_from_images_feature109.py case_root_dir
    return radiomics for each image saved as .npy.
    ```

3. Normalize train and test features.
    ```
    python normalize_train_and_test_from_previously_extracted_features.py extracted_features_path
    return Train_400_features_CV_and_CLINICAL.csv' and Test_300_features_CV_and_CLINICAL.csv (in /normalized_features_csv/)
    ```
4. Run models to train and test.
    ```
    python DCIS_Train400_test300_save_cross-validated_probabilites_for_CI_and_odds_ratio.py 200
    return saved_logs_predictions_and_labels/Final_Test300_from_train400_pred_and_labels_4models_cvtop11_109CV_4Cli.csv
    ```
5. Statistical analysis for CI and p-values.
    ```
    ROC_CI_PRCurves_for_training_and_testing.R
    return ROC curves, p-values and confidence intervals for different models.
    ```

