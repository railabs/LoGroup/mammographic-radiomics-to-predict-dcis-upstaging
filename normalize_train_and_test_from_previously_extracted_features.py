## python -W ignore ..py
import sys,os,random,math,time,glob,csv,scipy.io,copy,xlrd,datetime,xlwt,dicom,pydicom,cv2
import numpy as np
from scipy import interp
import matplotlib.pyplot as plt
from itertools import cycle
from sklearn import svm, metrics, linear_model,preprocessing
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import RandomizedLogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from skimage.morphology import convex_hull_image
from skimage.measure import regionprops,label
from skimage.feature.texture import greycomatrix,greycoprops
from scipy.spatial.distance import pdist,cdist
from scipy.misc import imread,imsave

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
warnings.filterwarnings("ignore", category=RuntimeWarning)
warnings.filterwarnings("ignore", category=FutureWarning)
def mean_std_normalize(X_new_tr,X_new_val):
	mean_std_scaler0 = preprocessing.StandardScaler(copy=True, with_mean=True, with_std=True)
	X_tr = mean_std_scaler0.fit_transform(X_new_tr)
	if len(X_new_val)>0:
		X_val = mean_std_scaler0.transform(X_new_val)
	else:
		X_val = []
	return X_tr,X_val
#####################################################################################################################################################################
extracted_features_path  = ''
npy_dcis_path_all_ge_nonge = glob.glob(extracted_features_path + '*.npy')

xlsx_file_name_alldcis = 'normalized_features_csv/Original_DCIS_Train400_test300_diagnosis_and_clinical_information.xlsx'
cluster_features_names = ['MCC Area', 'MCC Eccentricity', 'MCC NO of MCs',  'MCC MCs Coverage', 'MCC Background Mean', 'MCC Background Std', 'MCC All MCs Intensity Mean', 'MCC All MCs Intensity Std', 'Mean GLCM Contrast', 'Mean GLCM Correlation', 'Mean GLCM Energy', 'Mean GLCM Homogeneity', 'Lesion Size']
ind_features_names = ['MC Perimeter', 'MC Area', 'MC Circularity', 'MC Eccentricity', 'MC Major Axis', 'MC Minor Axis', '1st Variant of Hu Moments','2nd Variant of Hu Moments','3rd Variant of Hu Moments','4th Variant of Hu Moments','5th Variant of Hu Moments','6th Variant of Hu Moments','7th Variant of Hu Moments','MC Distance to Cluster Centroid','MC Distance to Nearest MC','MC Normalized Degree','MC Intensity Mean','MC Intensity Std','MC Background Intensity Mean','MC Background Intensity Std','MC Mean GLCM Contrast','MC Mean GLCM Correlation','MC Mean GLCM Energy','MC Mean GLCM Homogeneity']
feature_names_all =	cluster_features_names[:-1] + [' '.join(['Mean',ss]) for ss in ind_features_names] + [' '.join(['Std',ss]) for ss in ind_features_names] + \
					[' '.join(['Min',ss]) for ss in ind_features_names] + [' '.join(['Max',ss]) for ss in ind_features_names] + \
					cluster_features_names[-1:]

num_features_new = len(feature_names_all)
dcis_file = xlrd.open_workbook(xlsx_file_name_alldcis)
dcis_training= dcis_file.sheet_by_index(0)
dcis_testing = dcis_file.sheet_by_index(1)
num_cli_features = 4

###*****####****DCIS
###*****####****DCIS


#################### Training Cases ########################################
item_list = [str(i.value) for i in dcis_training.row(0)]
dx_col = [idss for idss,ii in enumerate(item_list) if 'Diagnosis' in ii][0]
model_col = [idss for idss,ii in enumerate(item_list) if 'Manufacturer Model Name' in ii][0]
fakeid_col = [idss for idss,ii in enumerate(item_list) if 'Fake ID' in ii][0]
idx_four_clinical = [[idss for idss,ii in enumerate(item_list) if 'Nuclear Grade' in ii][0] , [idss for idss,ii in enumerate(item_list) if 'ER' in ii][0] ,[idss for idss,ii in enumerate(item_list) if 'PR' in ii][0] ,[idss for idss,ii in enumerate(item_list) if 'Age' in ii][0]]
##
training_names_with_dx_model_cli_feat = [[str(dcis_training.cell(idxx,fakeid_col).value),str(dcis_training.cell(idxx,dx_col).value), \
str(dcis_training.cell(idxx,model_col).value), float(dcis_training.cell(idxx,idx_four_clinical[0]).value), \
float(dcis_training.cell(idxx,idx_four_clinical[1]).value),float(dcis_training.cell(idxx,idx_four_clinical[2]).value), \
float(dcis_training.cell(idxx,idx_four_clinical[3]).value),] for idxx in range(1,len(dcis_training.col(fakeid_col)))]
training_names_with_dx_model_cli_feat = [[ii[0],0,ii[2],ii[3],ii[4],ii[5],ii[6]] if 'Pure' in ii[1] else [ii[0],1,ii[2],ii[3],ii[4],ii[5],ii[6]] for ii in training_names_with_dx_model_cli_feat]
########
X_training_400_cli_ori = np.array([]).reshape(0,num_cli_features)
X_training_400_ori = np.array([]).reshape(0,num_features_new)
Y_training_400_ori = np.array([])
model_training_400_ori = []
for one_name_list in training_names_with_dx_model_cli_feat:
	one_added_feat = [mat for mat in npy_dcis_path_all_ge_nonge if mat.split('/')[-1].split('_feat.npy')[0] in one_name_list[0]][0]
	X_one_feat = np.load(one_added_feat).reshape((1,num_features_new))
	X_training_400_ori = np.concatenate((X_training_400_ori,X_one_feat),axis = 0)
	one_dx = [int(ss[1]) for ss in training_names_with_dx_model_cli_feat if ss[0] in one_added_feat.split('/')[-1]]
	one_model = 'GE' if 'senograph' in one_name_list[2].lower() else 'NonGE'
	model_training_400_ori.append(one_model)
	Y_training_400_ori = np.concatenate((Y_training_400_ori,one_dx))
	one_cli = np.array(one_name_list[-4:]).reshape(1,4)
	X_training_400_cli_ori = np.concatenate((X_training_400_cli_ori,one_cli),axis = 0)

################# Rest Testing Cases ##################################
item_list = [str(i.value) for i in dcis_testing.row(0)]
dx_col = [idss for idss,ii in enumerate(item_list) if 'Diagnosis' in ii][0]
model_col = [idss for idss,ii in enumerate(item_list) if 'Manufacturer Model Name' in ii][0]
fakeid_col = [idss for idss,ii in enumerate(item_list) if 'Fake ID' in ii][0]
mrn_col = [idss for idss,ii in enumerate(item_list) if 'MRN' in ii][0]
idx_four_clinical = [[idss for idss,ii in enumerate(item_list) if 'Nuclear Grade' in ii][0] , [idss for idss,ii in enumerate(item_list) if 'ER' in ii][0] ,[idss for idss,ii in enumerate(item_list) if 'PR' in ii][0] ,[idss for idss,ii in enumerate(item_list) if 'Age' in ii][0]]

testing_names_with_dx_model_cli_feat = [[str(dcis_testing.cell(idxx,fakeid_col).value),str(dcis_testing.cell(idxx,dx_col).value),str(dcis_testing.cell(idxx,model_col).value), float(dcis_testing.cell(idxx,idx_four_clinical[0]).value),float(dcis_testing.cell(idxx,idx_four_clinical[1]).value),float(dcis_testing.cell(idxx,idx_four_clinical[2]).value),float(dcis_testing.cell(idxx,idx_four_clinical[3]).value),] for idxx in range(1,len(dcis_testing.col(fakeid_col)))]
testing_names_with_dx_model_cli_feat= [[ii[0],0,ii[2],ii[3],ii[4],ii[5],ii[6]] if 'Pure' in ii[1] else [ii[0],1,ii[2],ii[3],ii[4],ii[5],ii[6]] for ii in testing_names_with_dx_model_cli_feat]
########
X_testing_300_cli_ori = np.array([]).reshape(0,num_cli_features)
X_testing_300_ori = np.array([]).reshape(0,num_features_new)
Y_testing_300_ori = np.array([])
model_testing_300_ori = []
for one_name_list in testing_names_with_dx_model_cli_feat:
	one_added_feat = [mat for mat in npy_dcis_path_all_ge_nonge if mat.split('/')[-1].split('_feat.npy')[0] in one_name_list[0]][0]
	X_one_feat = np.load(one_added_feat).reshape((1,num_features_new))
	X_testing_300_ori = np.concatenate((X_testing_300_ori,X_one_feat),axis = 0)
	one_dx = [int(ss[1]) for ss in testing_names_with_dx_model_cli_feat if ss[0] in one_added_feat.split('/')[-1]]
	one_model = 'GE' if 'senograph' in one_name_list[2].lower() else 'NonGE'
	model_testing_300_ori.append(one_model)
	Y_testing_300_ori = np.concatenate((Y_testing_300_ori,one_dx))
	one_cli = np.array(one_name_list[-4:]).reshape(1,4)
	X_testing_300_cli_ori = np.concatenate((X_testing_300_cli_ori,one_cli),axis = 0)

#########################################################################################################################

X_ge_all = np.concatenate((	X_training_400_ori[np.array([idss for idss, ii in enumerate(model_training_400_ori) if ii=='GE']),:],\
	X_testing_300_ori[np.array([idss for idss, ii in enumerate(model_testing_300_ori) if ii=='GE']),:]), axis = 0)
X_nonge_all = np.concatenate((X_training_400_ori[np.array([idss for idss, ii in enumerate(model_training_400_ori) if ii=='NonGE']),:],\
	X_testing_300_ori[np.array([idss for idss, ii in enumerate(model_testing_300_ori) if ii=='NonGE']),:]), axis = 0)

##
[X_ge_all_prenormed,X_training_400_ge] = mean_std_normalize(X_ge_all,X_training_400_ori[np.array([idss for idss, ii in enumerate(model_training_400_ori) if ii=='GE']),:])
[X_nonge_all_prenormed,X_training_400_nonge] = mean_std_normalize(X_nonge_all,X_training_400_ori[np.array([idss for idss, ii in enumerate(model_training_400_ori) if ii=='NonGE']),:])
X_training_400 = np.concatenate((X_training_400_ge,X_training_400_nonge),axis= 0)
Y_training_400 = np.concatenate((Y_training_400_ori[np.array([idss for idss, ii in enumerate(model_training_400_ori) if ii=='GE'])],\
	Y_training_400_ori[np.array([idss for idss, ii in enumerate(model_training_400_ori) if ii=='NonGE'])]),axis= 0)
info_training_400 = [training_names_with_dx_model_cli_feat[idss] for idss, ii in enumerate(model_training_400_ori) if ii=='GE'] + [training_names_with_dx_model_cli_feat[idss] for idss, ii in enumerate(model_training_400_ori) if ii=='NonGE']
##
[X_ge_all_prenormed,X_testing_300_ge] = mean_std_normalize(X_ge_all,X_testing_300_ori[np.array([idss for idss, ii in enumerate(model_testing_300_ori) if ii=='GE']),:])
[X_nonge_all_prenormed,X_testing_300_nonge] = mean_std_normalize(X_nonge_all,X_testing_300_ori[np.array([idss for idss, ii in enumerate(model_testing_300_ori) if ii=='NonGE']),:])
X_testing_300 = np.concatenate((X_testing_300_ge,X_testing_300_nonge),axis= 0)
Y_testing_300 = np.concatenate((Y_testing_300_ori[np.array([idss for idss, ii in enumerate(model_testing_300_ori) if ii=='GE'])],\
	Y_testing_300_ori[np.array([idss for idss, ii in enumerate(model_testing_300_ori) if ii=='NonGE'])]),axis= 0)
info_testing_300 = [testing_names_with_dx_model_cli_feat[idss] for idss, ii in enumerate(model_testing_300_ori) if ii=='GE'] + [testing_names_with_dx_model_cli_feat[idss] for idss, ii in enumerate(model_testing_300_ori) if ii=='NonGE']

#########################################################################################################################
#########################################################################################################################

f_feat_training_400 = open('normalized_features_csv/Train_400_features_CV.csv','w')
wri = csv.writer(f_feat_training_400)
wri.writerow(['Case Name','Label'] + new_feat_list)
_= [wri.writerow(info_training_400[idss][:2]+one_feat) for idss,one_feat in enumerate(X_training_400.tolist())]
f_feat_training_400.close()
##
f_feat_testing_300 = open('normalized_features_csv/Test_300_features_CV.csv','w')
wri = csv.writer(f_feat_testing_300)
wri.writerow(['Case Name','Label'] + new_feat_list)
_= [wri.writerow(info_testing_300[idss][:2]+one_feat) for idss,one_feat in enumerate(X_testing_300.tolist())]
f_feat_testing_300.close()

#########################################################################################################################
#########################################################################################################################
clinical_feat_list = ['Nuclear Grade','ER','PR','Age']
age_feat_all = [ii[3] for ii in X_training_400_cli_ori] + [ii[3] for ii in X_testing_300_cli_ori]
age_one_feat_all_train = np.array(X_training_400_cli_ori)[:,3]
[_,age_one_feat_train_prenormed] = mean_std_normalize(np.array(age_feat_all).reshape(-1,1),age_one_feat_all_train.reshape(-1,1))
age_one_feat_all_test = np.array(X_testing_300_cli_ori)[:,3]
[_,age_one_feat_test_prenormed] = mean_std_normalize(np.array(age_feat_all).reshape(-1,1),age_one_feat_all_test.reshape(-1,1))
#########
missing_labels = 100
ng_feat_no_missing_cases = [ii[0] for ii in X_training_400_cli_ori if ii[0]!=missing_labels] + [ii[0] for ii in X_testing_300_cli_ori if ii[0]!=missing_labels]
er_feat_no_missing_cases = [ii[1] for ii in X_training_400_cli_ori if ii[1]!=missing_labels] + [ii[1] for ii in X_testing_300_cli_ori if ii[1]!=missing_labels]
pr_feat_no_missing_cases = [ii[2] for ii in X_training_400_cli_ori if ii[2]!=missing_labels] + [ii[2] for ii in X_testing_300_cli_ori if ii[2]!=missing_labels]
#####################################################################################
ng_one_feat_all_train = np.array(X_training_400_cli_ori)[:,0]
er_one_feat_all_train = np.array(X_training_400_cli_ori)[:,1]
pr_one_feat_all_train = np.array(X_training_400_cli_ori)[:,2]
#################
# mean value for missing cases
ng_one_feat_all_train[np.where(ng_one_feat_all_train==100)] = np.mean(ng_feat_no_missing_cases)
er_one_feat_all_train[np.where(er_one_feat_all_train==100)] = np.mean(er_feat_no_missing_cases)
pr_one_feat_all_train[np.where(pr_one_feat_all_train==100)] = np.mean(pr_feat_no_missing_cases)
#################
# normalize training cases
[_,ng_one_feat_train_prenormed] = mean_std_normalize(np.array(ng_feat_no_missing_cases).reshape(-1,1),ng_one_feat_all_train.reshape(-1,1))
[_,er_one_feat_train_prenormed] = mean_std_normalize(np.array(er_feat_no_missing_cases).reshape(-1,1),er_one_feat_all_train.reshape(-1,1))
[_,pr_one_feat_train_prenormed] = mean_std_normalize(np.array(pr_feat_no_missing_cases).reshape(-1,1),pr_one_feat_all_train.reshape(-1,1))
#################
ng_one_feat_all_test = np.array(X_testing_300_cli_ori)[:,0]
er_one_feat_all_test = np.array(X_testing_300_cli_ori)[:,1]
pr_one_feat_all_test = np.array(X_testing_300_cli_ori)[:,2]
#################
# mean value for missing cases
ng_one_feat_all_test[np.where(ng_one_feat_all_test==100)] = np.mean(ng_feat_no_missing_cases)
er_one_feat_all_test[np.where(er_one_feat_all_test==100)] = np.mean(er_feat_no_missing_cases)
pr_one_feat_all_test[np.where(pr_one_feat_all_test==100)] = np.mean(pr_feat_no_missing_cases)
#################
# normalize testing cases
[_,ng_one_feat_test_prenormed] = mean_std_normalize(np.array(ng_feat_no_missing_cases).reshape(-1,1),ng_one_feat_all_test.reshape(-1,1))
[_,er_one_feat_test_prenormed] = mean_std_normalize(np.array(er_feat_no_missing_cases).reshape(-1,1),er_one_feat_all_test.reshape(-1,1))
[_,pr_one_feat_test_prenormed] = mean_std_normalize(np.array(pr_feat_no_missing_cases).reshape(-1,1),pr_one_feat_all_test.reshape(-1,1))
####################################################
####################################################

X_training_400_cli_prenormed = np.concatenate((ng_one_feat_train_prenormed,er_one_feat_train_prenormed,pr_one_feat_train_prenormed,age_one_feat_train_prenormed),axis = 1)
X_training_400_cli = np.concatenate((X_training_400_cli_prenormed[np.array([idss for idss, ii in enumerate(model_training_400_ori) if ii=='GE']),:],\
	X_training_400_cli_prenormed[np.array([idss for idss, ii in enumerate(model_training_400_ori) if ii=='NonGE']),:]),axis= 0)

X_testing_300_cli_prenormed = np.concatenate((ng_one_feat_test_prenormed,er_one_feat_test_prenormed,pr_one_feat_test_prenormed,age_one_feat_test_prenormed),axis = 1)
X_testing_300_cli = np.concatenate((X_testing_300_cli_prenormed[np.array([idss for idss, ii in enumerate(model_testing_300_ori) if ii=='GE']),:],\
	X_testing_300_cli_prenormed[np.array([idss for idss, ii in enumerate(model_testing_300_ori) if ii=='NonGE']),:]),axis= 0)

#####################################################################################
X_training_400_cv_and_cli =np.concatenate((X_training_400,X_training_400_cli),axis = 1) 
X_testing_300_cv_and_cli = np.concatenate((X_testing_300,X_testing_300_cli), axis = 1)

f_feat_training_400_cli_and_cv = open('normalized_features_csv/Train_400_features_CV_and_CLINICAL.csv','w')
wri = csv.writer(f_feat_training_400_cli_and_cv)
wri.writerow(['Case Name','Label'] + new_feat_list + clinical_feat_list )
_= [wri.writerow(info_training_400[idss][:2]+one_cv_and_cli) for idss,one_cv_and_cli in enumerate(X_training_400_cv_and_cli.tolist())]
f_feat_training_400_cli_and_cv.close()
##
f_feat_testing_300_cli_and_cv = open('normalized_features_csv/Test_300_features_CV_and_CLINICAL.csv','w')
wri = csv.writer(f_feat_testing_300_cli_and_cv)
wri.writerow(['Case Name','Label'] + new_feat_list + clinical_feat_list)
_= [wri.writerow(info_testing_300[idss][:2]+one_cv_and_cli) for idss,one_cv_and_cli in enumerate(X_testing_300_cv_and_cli.tolist())]
f_feat_testing_300_cli_and_cv.close()

